\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{4}
\contentsline {section}{\numberline {2}Task}{4}
\contentsline {section}{\numberline {3}Solution}{4}
\contentsline {section}{\numberline {4}Testing}{5}
\contentsline {subsection}{\numberline {4.1}exit}{5}
\contentsline {subsection}{\numberline {4.2}cd}{5}
\contentsline {subsection}{\numberline {4.3}checkEnv}{5}
\contentsline {subsection}{\numberline {4.4}Foreground and Background processes}{6}
\contentsline {subsection}{\numberline {4.5}Zombie processes}{7}
\contentsline {subsubsection}{\numberline {4.5.1}Detection by signals}{7}
\contentsline {subsubsection}{\numberline {4.5.2}Detection by polling}{7}
\contentsline {section}{\numberline {5}Access to code}{7}
\contentsline {subsection}{\numberline {5.1}Compilation}{7}
\contentsline {section}{\numberline {6}Reflection}{8}
\contentsline {section}{\numberline {7}Appendix}{8}
