\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{verbatim}
\usepackage{mathtools}
\usepackage[english]{babel}

% klistra in kod
\usepackage{listings}
\usepackage{color}

\title{Operativsytem : Laboration}
\usepackage{graphicx}
\author{Mark Wong \\ 930401-4195 \\ markwong@kth.se \and Samiul Alam \\ 930117-3499 \\ samiula@kth.se}
\begin{document}

\maketitle


\newpage
	\tableofcontents
\newpage

\newpage
	\begin{abstract}
	A unix shell is a command line interpreter that provides a text-based user interface to the unix interface. It can be seen as an abstraction of a subset of the OS system calls that are useful to the user. This report provides a solution to the task of writing a simple shell. The shell allows for standard commands such as \texttt{ls}, \texttt{cd} and \texttt{pwd} as well as a custom command \texttt{checkEnv} that runs \texttt{printenv | sort | pager} where \texttt{pager} = \texttt{less} or \texttt{more}. The report also describes how the implementation of the shell has been tested. Finally a reflection of the task is presented by the authors.
	\end{abstract}
\newpage
\renewcommand{\abstractname}{Summary}

\section{Introduction}
This is a lab exercise in the ID2200 course at KTH with the purpose of teaching students about how operating systems work. The lab covers the concepts: pipes, processes, system calls etc. and how these together can be used to create a simple shell.

\section{Task}
The aim of the lab is to write a small shell in C that implements the built-in commands exit and cd. When a user enters the command \texttt{checkEnv} the following shall happen: \texttt{printenv | sort | pager}. If the user supplies arguments to said command the following shall happen instead: \texttt{printenv | grep <arguments> | sort | pager}. The \textit{pager} executed will primarily be based on the value of the users PAGER environment. If no such variable is set the program shall execute \texttt{less} and if that fails \texttt{more}. The program shall handle execution of processes in foreground and background. The program shall also correctly detect terminated background processes with two mechanisms that can be chosen at compilation time. The mechanisms are ordinary polling and detection by signals. 

\section{Solution}
The program contains an infinite loop that begins by reading input from the user. Depending on the input different actions will be executed. The loop breaks when the user enters \texttt{exit}. Every time we loop we check for terminated backgroundproccesses with \texttt{waitpid()} with \texttt{WNOHANG}. If the user enters cd
and does not supply any argument or an invalid argument we call \texttt{chdir()} with the \textit{HOME}. If the user enters a valid path we create a childprocess with \texttt{fork()} and execute the request to change directory with \texttt{execvp}. 
\\
\\
If a command is to be run in the foreground we start the timer and let the child process run the command using \texttt{execvp}. The parent process uses \texttt{waitpid()} to wait for the child process to finish. When the child process terminates it signals the parent process. The program reaps the child process and then prints a termination message that contains the process id of the child reaped.  
\\
\\
If a command is to be run in the background the shell prints a message containing the process id and that the has started in the background. The parent does not wait for the child process. Instead the shell reaps the dead child-process either by polling or by detecting the signal that the child sends when it terminates.
\\
\\
To avoid a \texttt{signal} \texttt{waitpid()} race when running a foreground process (i.e. foreground processes being reaped by the signal handler), the parent of a foreground child-process uses \texttt{sigprocmask} to block its child's signal so the parent itself can use \texttt{waitpid} to reap the dead process. Then the parent unblocks the signals so other processes' signals can come through.

\section{Testing}

\subsection{exit}
a built-in command \texttt{exit} which terminates all remaining processes started from the shell in an orderly manner before exiting the shell itself.
\\
\\
As explained above, the shell kills all processes that have been started by the shell before exiting the shell. To test this, multiple instances of the java program Test was started as background processes by the shell. The shell was then given the command \texttt{exit} and then the command \texttt{ps} was run in bash to check if any of the processes started by the shell were still running. This test showed that no processes were still running after the shell had been terminated.
\\
\\
This is all done in valgrind which indicates that no memory leaks are possible.

\subsection{cd}
A built-in command \texttt{cd} which should behave exactly as a normal built-in command \texttt{cd} with the exception that you do not need explicitly to update environment variables.
\\
\\
Various arguments are given with the cd command. Giving it a directory that does not exist makes it go to the directory specified by the HOME environment variable. \texttt{cd ..} makes it move up a directory. To make sure that the working directory is actually changing we double check by runnning \texttt{ls} and \texttt{pwd} to list the files where we (supposedly) are and to get the actual path of the working directory.
\\
\\
Valgrind inidicates that no memory leaks are possible when using these commands.

\subsection{checkEnv}
A built-in command \texttt{checkEnv} that executes \texttt{printenv | sort | pager}. If the user supplies arguments to said command the following shall happen instead: \texttt{printenv | grep <arguments> | sort | pager}. The \textit{pager} executed will primarily be based on the value of the users PAGER environment. If no such variable is set the program shall execute \texttt{less} and if that fails \texttt{more}.
\\
\\
checkEnv without arguments behave exactly as specified. The environment is sorted and then displayed with a pager. On our working stations no pager is defined and was therefore displayed with \texttt{less}.
\\
\\
checkEnv with arguments also behave as specified. \texttt{Grep} is used on the output from \texttt{printenv}. The output from \texttt{grep} is then sorted and then printed using a pager. One of the executed test was \texttt{checkEnv PROGRAM} which correctly displayed only the lines containing PROGRAM. 
\\
\\
Valgrind indicates that no memory leaks are possible when using these commands.

\subsection{Foreground and Background processes}
Testing that a foreground process blocks any other commands is done by running the java program Test. This causes the shell to block any input from the user while running the program. You can also run programs like nano. The same behaviour should be seen in any arbitrary program that runs in the foreground. Wallclock time for the java program is around 10,000ms as it should be. When it is finished the shell prints the appropriate information.
\\
\\
Running the command \texttt{java Test \&} causes the shell to execute the program in the background. While it runs other commands can be run. After 10s the shell prints a message telling us that the process has finished along with its process id.
\\
\\
Example of foreground process execution:
\begin{verbatim}
OS-Shell v1.0 >> java Test
Foreground process 5242 terminated
wallclock time: 10169.123000
OS-Shell v1.0 >>
\end{verbatim}
\newline
\\
\\
\\
Example of background process execution:
\begin{verbatim}
OS-Shell v1.0 >> java Test &
wallclock time: 0.297000
OS-Shell v1.0 >> ps
  PID TTY           TIME CMD
 5167 ttys000    0:00.06 -bash
 5241 ttys000    0:00.01 ./shell.out
 5252 ttys000    0:00.15 /usr/bin/java Test &
 4211 ttys001    0:00.24 -bash
 5162 ttys001    0:00.13 ssh samiula@shell.it.kth.se
Foreground process 5253 terminated
wallclock time: 10.150000
OS-Shell v1.0 >> 
Background process 5252 finished
OS-Shell v1.0 >> ps
  PID TTY           TIME CMD
 5167 ttys000    0:00.06 -bash
 5241 ttys000    0:00.01 ./shell.out
 4211 ttys001    0:00.24 -bash
 5162 ttys001    0:00.13 ssh samiula@shell.it.kth.se
Foreground process 5263 terminated
wallclock time: 9.127000
OS-Shell v1.0 >>
\end{verbatim}
\newline
\\
Valgrind inidicates that no memory leaks are possible when using these commands.

\subsection{Zombie processes}
The java program Test has been used to ensure that any zombie processes are killed by the shell with polling and signals respectively. Multiple background processes running Test were started by the shell by running the command \texttt{java Test \&} and then multiple foreground processes were executed by running commands like ls or pwd. After 10 seconds had passed (the total running time for the program Test) we ran ps to see if any zombie processes were still there.
\subsubsection{Detection by signals}
All zombie processes were reaped as they signaled that they had terminated.
\subsubsection{Detection by polling}
All zombie processes were taken reaped as the shell polled for dead child-processes at the start of each loop before waiting for input.

\section{Access to code}
Our code can be accessed at shell.ict.kth.se. The files are placed in the \texttt{OSLab} directory.
\subsection{Compilation}
To compile the shell using polling for detection of terminated procceses:

\begin{verbatim}
$gcc -pedantic -Wall -ansi -O4 shell.c -o shell.out
\end{verbatim}
To compile the shell using signals for detection of terminated procceses:

\begin{verbatim}
$gcc -pedantic -Wall -DSIGSET=1 -ansi -O4 shell.c -o shell.out
\end{verbatim}

\section{Reflection}
This lab was a great exercise that introduced us to concepts in the realm of operating systems. We have learned how to create foreground and background processes. We have also learned how to detect and remove terminated proccess. The time used to finish the lab was approximately 30 hours. The time used to finish the report was approximately 5 hours.

\section{Appendix}



\end{document}