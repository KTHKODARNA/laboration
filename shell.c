#define _XOPEN_SOURCE 500
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <sys/time.h>
#include <signal.h>
#include <sys/wait.h>

#define PIPE_READ 0
#define PIPE_WRITE 1

/* Pipes */
int p1[2], p2[2], p3[2];
char *exec[2];

/**
 * Prints wallclock time.
 */
void printTime(struct timeval tv1, struct timeval tv2) {
	double elapsedTime;
	elapsedTime = (tv2.tv_sec - tv1.tv_sec) * 1000.0;
	elapsedTime += (tv2.tv_usec - tv1.tv_usec) / 1000.0;
	printf("wallclock time: %f\n", elapsedTime );
}

/* Signal handler that ignores the interrupt signal */
void INThandler(int sig) {
    if(signal(sig, SIG_IGN) == SIG_ERR) {
    	perror("Signal error:");
    }
}

/* Prints the shell prompt */
void prompt() {
	fprintf(stdout, "OS-Shell v1.0 >> ");
	fflush(stdout);
}

/* Reaps child processes when they send a signal that they terminated */
void handle_sigchld(int sig) {
	pid_t pid;
	int status;
	while ((pid = waitpid((pid_t)(-1), &status, WNOHANG)) > 0) {
		if(pid == -1) {
			perror("waitpid failed:");
		}
		printf("Background process %d finished\n", pid);
	}
}

/* Sets up handle_sigchld as the signal handler for when child processes terminate */
void reaper() {
	struct sigaction sa;
	sa.sa_handler = &handle_sigchld;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART | SA_NOCLDSTOP;
	if (-1 == sigaction(SIGCHLD, &sa, 0)) {
		perror(0);
		exit(1);
	}
}

/**
 * The main shell.
 * Commands:
 * "exit" kills any running processes and exits the shell.
 * "cd" followed by a directory changes the working directory to the specified directory. If the directory does not exist
 * the working directory is changed to the HOME directory.
 * "checkEnv" executes the command printenv | sort | pager where "pager" is less or more.
 * "checkEnv" followed by arguments executes "printenv | grep <arguments> | sort | pager"
 * when compiling, -DSIGDET=1 makes the shell reap child-processes with a signal handler.
 * otherwise the shell reaps child-processes by polling.
 */
int main(int argc, char *argv[]) {
	int status;
	pid_t pid, pid1, pid2, pid3, pid4;
	struct timeval tv1, tv2;
	char input[80];
	int len;
	char *temp;
	char *pagerenv;
	sigset_t signal_set;
	sigemptyset(&signal_set);
	sigaddset(&signal_set, SIGCHLD);

	#if SIGDET
		signal(SIGCHLD, reaper);
	#endif

	pagerenv = getenv("PAGER");

	/* Read commands in a loop */
	while(1) {

		char *args[64];
		char **next = args;
		len = 0;
		signal(SIGINT, INThandler);

		/* Reap zombie-processes with polling */
		#if SIGDET == 0
			while ((pid = waitpid(-1, &status, WNOHANG)) > 0) {
				if(pid == -1) {
					perror("waitpid failed:");
				}
				printf("Background process %d finished\n", pid);
				fflush(stdout);
			}
		#endif

		prompt();
		
		fgets(input, sizeof(input), stdin);

		/* Split arguments into array args */
		temp = strtok(input, " \n");
		while(temp != NULL) {
			*next++ = temp;
			temp = strtok(NULL, " \n");
			len++;
		}
		*next = NULL;
		if(len > 0) {
			/* exit */
			if(0 == strcmp(input, "exit")) {
				kill(0, SIGTERM);

			} else if(strcmp(args[0], "checkEnv") == 0) {
				gettimeofday(&tv1, NULL);
				if(args[1] != NULL) {
					if(strcmp(args[1], "") != 0) {
					args[len] = NULL;
					len++;
						/* WITH GREP */
						/* If pager is unavailable, set pager variable to less */
						if (pagerenv == NULL) {
							pagerenv = "less";
						}

						if(-1 == pipe(p1)) {
							perror("pipe1 failed:");
						}

						if(-1 == pipe(p2)) {
							perror("pipe2 failed:");
						}

						if(-1 == pipe(p3)) {
							perror("pipe3 failed:");
						}

						pid1 = fork();
						if(pid1 == -1) {
							perror("fork1");
							exit(EXIT_SUCCESS);
						} else if (pid1 == 0) {
							/* child */

							if(-1 == dup2(p1[PIPE_WRITE], STDOUT_FILENO)) {
								perror("dup2 error:");
							}

							/* Call printenv and print to stdout */
							if(execlp("printenv", "printenv", NULL) == -1) {
								perror("printenv failed:");
							}

						} else {
							/* parent */
							if(-1 == close(p1[PIPE_WRITE])) {
								perror("close failed:");
							}
							if(-1 == waitpid(0, NULL, 0)) {
								perror("waitpid failed:");
							}

							pid2 = fork();

							if(pid2 == -1) {
								perror("fork2:");
							} else if(pid2 == 0) {

								/* child - GREP */
								if(-1 == dup2(p1[PIPE_READ], STDIN_FILENO)) {
								perror("dup2 error:");
								}
								if(-1 == dup2(p2[PIPE_WRITE], STDOUT_FILENO)) {
								perror("dup2 error:");
								}

								if(-1 == execvp("grep", args)) {
									perror("grep failed:");
								}

							} else {
								/* parent */
								if(-1 == close(p2[PIPE_WRITE])) {
									perror("close failed:");
								}
								if(-1 == waitpid(0, NULL, 0)) {
									perror("waitpid failed:");
								}
								pid3 = fork();

								if(pid3 == -1) {
									perror("fork3:");
								} else if(pid3 == 0) {
									/* child - SORT */
									if(-1 == dup2(p2[PIPE_READ], STDIN_FILENO)) {
										perror("dup2 error:");
									}

									if(-1 == dup2(p3[PIPE_WRITE], STDOUT_FILENO)) {
										perror("dup2 error:");
									}

									if(-1 == execlp("sort", "sort", NULL)) {
										perror("sort failed:");
									}

								} else {
									/* parent */
									if(-1 == close(p3[PIPE_WRITE])) {
										perror("close failed:");
									};
									waitpid(0, NULL, 0);

									pid4 = fork();

									if(pid4 == -1) {
										perror("fork4:");
									} else if(pid4 == 0) {
										/* child - PAGER */
										if(-1 == dup2(p3[PIPE_READ], STDIN_FILENO)) {
											perror("dup2 error:");
										}

										exec[0] = pagerenv;
										exec[1] = NULL;
										if (-1 == execvp(exec[0], exec)){
									      perror("pager failed:");
										}
										/* less fanns ej. Kör more */			
										exec[0] = "more";
										if (-1 == execvp(exec[0], exec)){
									      perror("pager failed:");
										}
									} else {
										/* parent */
										if(-1 == waitpid(0, NULL, 0)) {
											perror("waitpid failed:");
										}
									}
								}
							}
						}
					}

				} else {
					/* WITHOUT GREP */
					/* If pager is unavailable, set pager variable to less */
					if (pagerenv == NULL) {
						pagerenv = "less";
					}

					if(-1 == pipe(p1)) {
						perror("pipe1 failed:");
					}

					if(-1 == pipe(p2)) {
						perror("pipe2 failed:");
					}

					pid1 = fork();
					if(pid1 == -1) {
						perror("fork1");
						exit(EXIT_SUCCESS);
					} else if (pid1 == 0) {
						/* child */

						if(-1 == dup2(p1[PIPE_WRITE], STDOUT_FILENO)) {
							perror("dup2 error:");
						}

						/* Call printenv and print to stdout */
						if(execlp("printenv", "printenv", NULL) == -1) {
							perror("printenv failed:");
						}

					} else {
						/* parent */

						if(-1 == close(p1[PIPE_WRITE])) {
							perror("close failed:");
						}
						if(-1 == waitpid(0, NULL, 0)) {
							perror("waitpid failed:");
						}
						pid2 = fork();

						if(pid2 == -1) {
							perror("fork2:");
						} else if(pid2 == 0) {
							/* child - SORT */

							if(-1 == dup2(p1[PIPE_READ], STDIN_FILENO)) {
								perror("dup2 error:");
							}

							if(-1 == dup2(p2[PIPE_WRITE], STDOUT_FILENO)) {
								perror("dup2 error:");
							}

							if(execlp("sort", "sort", NULL) == -1) {
								perror("sort failed:");
							}

						} else {
							/* parent */
							if(close(p2[PIPE_WRITE]) == -1) {
								perror("close failed:");
							}
							if(-1 == waitpid(0, NULL, 0)) {
								perror("waitpid failed:");
							}
							pid3 = fork();

							if(pid3 == -1) {
								perror("fork3:");
							} else if(pid3 == 0) {
								/* child - PAGER */
								if(-1 == dup2(p2[PIPE_READ], STDIN_FILENO)) {
									perror("dup2 error:");
								}

								exec[0] = pagerenv;
								exec[1] = NULL;
								if (-1 == execvp(exec[0], exec)){
							      perror("pager failed:");
								}
								/* less unavailable. Run with more */			
								exec[0] = "more";
								if (-1 == execvp(exec[0], exec)){
							      perror("pager failed:");
								}
							} else {
								/* parent */
								if(-1 == waitpid(0, NULL, 0)) {
									perror("waitpid failed:");
								}
							}

						}

					}

			}

			/* cd HOME */
			} else if( strcmp(args[0], "cd") == 0) {
				gettimeofday(&tv1, NULL);
				if(len > 1) {
					if(chdir(args[1]) == -1) {
					chdir(getenv("HOME"));
					perror("Error:");
					printf("Changing directory to HOME.\n");
					}
				} else {
					printf("Changing directory to HOME.\n");
					chdir(getenv("HOME"));
				}
			} else {
				/* Commands that start child-process (Fork) */
				gettimeofday(&tv1, NULL);
				pid = fork();
				if( pid == 0 ) {
					if(execvp(args[0], args) == -1) {
						perror("Error:");
						exit(0);
					}
				}
				else if( pid > 0 ) { /* Run in foreground */
					if(strcmp(args[len-1], "&") != 0) {
						sigprocmask(SIG_BLOCK, &signal_set, NULL);
						waitpid(0, &status, 0);
						printf("Foreground process %d terminated\n", pid);
						sigprocmask(SIG_UNBLOCK, &signal_set, NULL);
					} else { /* Run in background (do not wait) */
					}
				}
				else
					/* Check errno and print error message */
					perror("Error:");
			}
			/* prints */
			gettimeofday(&tv2, NULL);
			printTime(tv1, tv2);
		}
	}
	return EXIT_SUCCESS;
}
